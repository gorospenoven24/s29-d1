// require express module and save it in a const
const express = require("express");

// Create an application using express
const app = express();

// App server wil listen to port 3000
const port = 3000;

// Setup for allowing the server to  handle data from request
// allow the app to read json data
// using use() method--------------------------------------
app .use(express.json());

// allows the app to read data from forms
app.use(express.urlencoded({extended:true}));

// GET route
app.get("/",(req, res) =>{
	res.send("Hello World");
})

app.get("/hello",(req,res)=>{
	res.send("Heloo from the /hello endpoint!")
})

// POST route - to create
app.post("/hello", (req,res)=>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

//Mock database
let users = [];

 //will create signup post 
 app.post("/signup",(req,res)=>{
 	console.log(req.body);
 	if(req.body.username !== '' && req.body.password !== ''){
 		users.push(req.body);
 		res.send(`usr ${req.body.username} successfully registered`);
 	}
 	else{
 		res.send("Please input BOTH username anf password");
 	}
 })

 // PUT request for changing the password
 app.put("/change-password",(req,res)=>{
 	// createsa variable to store the message to be sent back to the client

 	let message;

 	// create a for loop that will loop through the elements of the "users" array
 	for(let i = 0; i < users.length; i++){
 		if (req.body.username === users[i].username){

 			users[i].password = req.body.password;
 			message = `User ${req.body.username}'s passord has been updated`;
 			break;
 		}
 		else{
 			message = "Users does not exist"
 		}
 	}

 	res.send(message);
 })


/////////////////activity//////////////////////////////////////

 // num 2
 app.get("/home",(req,res)=>{
	res.send("Welcome to the Homepage")
})


 // num 3
 app.get("/users",(req,res)=>{
	res.send(users)
})


 app.delete("/delete-user",(req,res)=>{
 	// createsa variable to store the message to be sent back to the client

 	let message;

 	// create a for loop that will loop through the elements of the "users" array
 	for(let i = 0; i < users.length; i++){
 		if (req.body.username === users[i].username){

 			users[i].password = req.body.password;
 			 users.splice(req.body, 1);
 			message = `User ${req.body.username}'s has been deleted`;
 			break;
 		}
 		else{
 			message = "Users does not exist"
 		}
 	}

 	res.send(message);
 })


// tells the server to listen to the port
// listen() method
app.listen(port,() => console.log(`Server is running at port ${port}`));



